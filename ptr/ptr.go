// Package ptr oneliners for pointers to primitives
package ptr

// Bool pointer to bool
func Bool(b bool) *bool {
	return &b
}

// String pointer to String
func String(s string) *string {
	return &s
}

// Int pointer to Int
func Int(i int) *int {
	return &i
}

// Float64 pointer to float64
func Float64(f float64) *float64 {
	return &f
}
